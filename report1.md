# Report : first lecture of Large Scale Data Management

Students: Ivana ADAM & Ilya Meignan--Masson

1. Definition of a distributed system
A distributed system is a system composed of a group of computing devices (computer, processor, ...) linked together with a network. They usually work towards a common goal to achieve faster computation.

2. Regarding process/machine failures, what is :

a) A crash fault

A crash fault of a distributed system happens when a component or a network link fails tampering which impacts the rest of the system. This may not be detected during the executioni.

Correction : Correct machines never fails. Faulty machines will fail at some point and not recover.
    
b) A crash-recovery fault

A crash-recovery fault is a crash fault that the system is able to handle either by gracefully stopping its execution or by ignoring the error and continuing the execution.

Correction : A crash-recovery model means that a machine can either be correct (does not crash or crashes and recovers a finite number of time).

c) A Byzantine fault 
    
A byzantine fault in a distributed system happens when some components might have failed but the rest of the system have limited or unreliable information about the failure. The component exhibits an arbitrary behaviour where it can respond correctly or omit some intended steps. 

Correction : Byzantine machine can do anything. Send wrong messages, lie, forget to send messages.

3. Regarding communication links between processes/machines, what is:

a)  Reliable communication channel

A channel is reliable if all the packets going through are all sent, in the right order and unmodified.

Correction : In the crash model. Correct machine will keep resending the messages until it receives the acknoledgment for the message. 

b)  Fair-loss communication channel

A channel is considered fair-loss if it can lose or repeat a finite number of packets.

Correction : A correct process sending a message an infinite number of time, the receiver will receive an infinite number of packets. 

c) Synchronous communication model

In a synchronous model, the communication must happen within a certain time window. This window has a known upper bound limit. 

d) An asynchronous communication model

As opposition to the synchronous model, the asynchronous communication does not take time into account, meaning that the time of receiving the message is not bound.

e) A partially synchronous communication model

The partially synchronous communication model is a middle ground between synchronous and asynchronous model. Its models a system which is asynchronous at start and which becomes synchronous. It takes the assumption that the bound of the synchronous model (B) exist and adds an upperbound on time for the synchronization of the system (the Global Stabilization Time, or GST). Any message sent at time t must be received before max(t, GST) + B. This means that if t < GST, then the message can arrive at last after GST + B. If t > GST, then the system is synchronous so the message will arrive at last at t + B.

Correction : In the real world, alternates between moments when the communication is synchronous and moments when the communication is asynchronous. 

4. Regarding the specification of a Distributed System abstraction, what is :

a)  A liveness property ? (give a concrete example)

The liveness property ensure that all the ressources are making progress at some point of the execution. Meaning that, on an execution that is not supposed to end, there are no ressources that end stuck at some point.

For example, if we take a Read/Write file server, all the components that connect to it should be able to Read or Write in a finite time (if possible quick for everyone) and not having everyone else having the priority over one instance, leading it to never be executed.

b) A safety property ? (give a concrete example)

One of the safety property is there is no instant in time where multiple components are in the same critical section.

Correction : no problems will appear during the execution.

An example can be drawn from the same situation as the previous question where there is a Read/Write file server. The safety property we described would ensure that there is no multiple writers or a writer and a reader at the same time accessing the server.


5. Regarding Failure Detectors:

a)  What is a Failure Detector 

A failure detector is an application responsible for the detection of crashes and faults of the components of the distributed system.
    
b) Give the specification of a Perfect Failure Detector (P)

A perfect Failure detector combines the strong completeness and the strong accuracy properties. This means that after a certain amount of time :

 - All  faulty components are suspected by all the other components.

 - None of the correct components are suspected while they are correct.
