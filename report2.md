# Report : second lecture of Large Scale Distributed system

Students : Ivane ADAM & Ilya MEIGNAN--MASSON

### 1. Regarding communications links between processes :


a) Design an algorithm implementing a reliable communication channel

The sender sends a stream of the same message until it receives an acknowledgment.

When the receiver receive a message, it send an acknowledgment and if the message has already been received, it drop it after sending the acknowledgment.


b) Assumptions :

 - Node can fail by crashing (crash failure model)
 - there exists a fair-loss communication channel

c) Write a proof that the algorithm you designed implement a reliable communication channel

Because the communication channel follows the fair-loss model, if the two processes are correct, sending an infinite number of messages will result in a infinite number of messages received. If the receiver receives an infinite number of messages, then it will send an infinite number of acknowledgements. Still under the fair-loss model, the sender will receive an infinite number of acks. So if both processes are correct, the sender will receive eventually an ack. 

Because the receiver drops the duplicate messages, the no duplication propertie will not be violated.

With an acknowledgement, the order of messages is preserved as following messages cannot be sent before the ack of the previous message is received.


### 2. Regarding Failures Detectors :

a) Design an algorithm implementing a Perfect Failure Detector

Assumptions :

- synchronous communication channel with a known upper bound of T.
- crash failure model
- stronger fair-loss -> The maximum amount of message that can be dropped in a row is L.

Algorithm :

Every process broadcast a heart-beat message every T/2. If a process P1 doesn't receive a heart-beat from another process P2 for L*T/2 amount of time, then P1 considers that P2 has crashed.

b) Prove that the algorithm you designed implement a Perfect Failure Detector.

With the assumption that we have, we know that after L+1 non received message, a process is dead because we should have received at least one message from it. Because of this, all the crashed processes will be detected at worst after L+1 non-received messages. 

Because a node only suspect another node after L+1 message, every process that received a message will not be suspected as failed before they are actually failed.

Because our model is the crash failure model, a failed node cant recover and will not be valid again.

### 3. Regarding Broadcasting protocols :

a) Give the specification of a Best-effort Broadcasting protocol

Best-effort broadcast guarantees that all correct processes deliver the same set of message.

Another guarantee is that no message is delivered more than once.

One final guarantee that we require is that if a process p delivers a message m with sender s, then m was previously broadcast by p.

b) Give the specification of a Reliable Broadcasting protocol

We can find multiple definition for reliable broadcast : 

- reliable broadcast without other guarantees. If a message is broadcasted by any of the nodes then all the other nodes will receive it.
- Fifo broadcast : if a node broadcast two different message m1 and m2, they must arrived in order : (m1 delivered before m2).
- Causal broadcast : If a message m1 is sent before m2 (not always from the same node), m1 must arrive before m2.
- Total order broadcast : On a node, if m1 arrive before a message m2, m1 must arrive before m2 for every other node.
- Fifo-total order broadcast: Fifo+total order broadcast.

c) Propose an algorithm implementing a Best-effort Broadcasting protocol

The sender process sends the message to all the other process including itself. As soon as a process receives a messages it delivers it.

d) Propose an algorithm implementing a reliable broadcasting protocol

A working but costly algorithm can be :

When a node receives a broadcasted message for the first time, it re-broadcasts it to the other nodes. With this, all the nodes that did not crash will receive the message.

It is expensive because N nodes will exchange n\u00b2 messages.

