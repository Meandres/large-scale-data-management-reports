# Produce PDFs from all Markdown files in a directory
# Inspired by Lincoln Mullen over at https://lincolnmullen.com/blog/a-makefile-to-convert-all-markdown-files-to-pdfs-using-pandoc/

PDFS := $(patsubst %.md,%.md.pdf,$(wildcard *.md))

all : $(PDFS)
                        
%.md.pdf : %.md
	pandoc $< -o $@ -V colorlinks -V urlcolor=NavyBlue -V geometry:"top=2cm, bottom=1.5cm, left=2cm, right=2cm"

clean :
	rm $(PDFS)

rebuild : clean all
